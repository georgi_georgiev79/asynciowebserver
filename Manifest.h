#ifndef MANIFEST_H_INCLUDED
#define MANIFEST_H_INCLUDED

#include <string>

/// input output parameter
#define IN
#define OUT

/// URL PARAM DEF
#define URL_PARAM_RETURN_URL        "returnURL"
#define URL_PARAM_USERID            "id"
#define URL_PARAM_ACTIVATION_CODE   "code"
#define URL_PARAM_USER_SHARE        "share"
#define URL_PARAM_HMAC              "hmac"
#define URL_PARAM_RESETPIN          "pinreset"
#define URL_PARAM_HEXID             "hexID"

#define RELEASE(pointer)  \
    if (pointer != NULL ) { \
        delete pointer;    \
        pointer = NULL;    \
    } \


const std::string const_string  = "";

#endif // MANIFEST_H_INCLUDED
