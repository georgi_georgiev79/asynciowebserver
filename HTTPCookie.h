#ifndef HTTPCOOKIE_H_INCLUDED
#define HTTPCOOKIE_H_INCLUDED

#include "Manifest.h"
#include <string>

using namespace std;

namespace HTTP {

class HTTPCookie  {

    string m_cookieID;
    string m_domain;
    string m_expire;

public:

    HTTPCookie ( );
    HTTPCookie (const string & aCookieID );

    void setCookieID (const string & aCookieID);
    void setDomain (const string & aDomain);
    void setExpirationDateUTC (const string & aExpire);
    void buildHTTPResponseHeaderFields( string & IN OUT aHttpResponseHeaderField );

};

}



#endif // HTTPCOOKIE_H_INCLUDED
