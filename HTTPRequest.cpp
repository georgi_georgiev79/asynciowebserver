#include "HTTPRequest.h"
#include <iostream>
#include "utilities.h"
#include "Manifest.h"

namespace HTTP {

HTTPRequest :: HTTPRequest () {
    m_pURL = new URL("");
}

HTTPRequest :: HTTPRequest (const HTTPRequest & aHttpRequest) {

    this->m_pURL = new URL( * aHttpRequest.m_pURL );
    this->m_postData = aHttpRequest.m_postData;
    this->m_httpProperties = aHttpRequest.m_httpProperties;

}

HTTPRequest & HTTPRequest :: operator= (const HTTPRequest & aHttpRequest) {

    if ( this == &aHttpRequest ) return *this;

    RELEASE( this->m_pURL );

    this->m_pURL = new URL( * aHttpRequest.m_pURL );
    this->m_postData = aHttpRequest.m_postData;
    this->m_httpProperties = aHttpRequest.m_httpProperties;

    return * this;
}

const URL & HTTPRequest :: Url() const {
    return * m_pURL;
}

void HTTPRequest :: Url ( const URL * apURL ) {

    if ( m_pURL == apURL ) return;
    RELEASE( m_pURL );
    m_pURL = (URL *)apURL;
}

const string & HTTPRequest :: operator [] (const string & aKey ) const {

    HTTPRequestProperties::const_iterator it  = m_httpProperties.find( aKey );
    if( m_httpProperties.end( ) != it ) {
        return it->second;
    }
    return const_string;
}

void HTTPRequest::setHttpPropertiesKeyWithValue (const string & aKey, const string & aValue) {

    if ( aKey.compare( HTTPREQUEST_COOKIE ) == 0 ) {

        size_t begin = aValue.find( "asid=" );
        if( string::npos != begin ) {
            string cookieName = aValue.substr( begin, 4 );
            if( !cookieName.compare( "asid" ) ) {
                begin += 5;
                m_httpProperties[aKey] = aValue.substr( begin, 36 );
            }
         }
    }  else {
       m_httpProperties[aKey] = aValue;
    }

    /// TODO :: add more processing properties
    // ... else if () {}
    // ...  else if () {}
}

void HTTPRequest :: setCookie(const string & cookieID) {
    m_httpProperties[ HTTPREQUEST_COOKIE ] = cookieID;
}

const string & HTTPRequest :: PostData() const {
    return m_postData;
}

void HTTPRequest :: PostData( const string & aPostData ) {
    m_postData = aPostData;
}

void HTTPRequest :: PostData ( const char * pBuff, size_t n ) {
    m_postData.assign(pBuff, n);
}

HTTPRequest :: ~HTTPRequest () {
    RELEASE(m_pURL);
}

}
