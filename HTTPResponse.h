#ifndef HTTPRESPONSE_H_INCLUDED
#define HTTPRESPONSE_H_INCLUDED

#include <string>
#include <map>
#include "HTTPCookie.h"

using namespace std;

namespace HTTP {

enum enCacheControl_t {
    enCacheControl_NoCache = 0,
    enCacheControl_Private,
    enCacheControl_Server,
    enCacheControl_ServerAndNoCache,
    enCacheControl_Public,
    enCacheControl_ServerAndPrivate
};


typedef map <string, string> HTTPResponseHeaders;


class HTTPCookie;

class HTTPResponse  {

    string m_responseStatusCode;
    string m_cacheControl;
    string m_contentType;
    string m_cookieHeaders;
    string m_headerFields;
    string m_responseBody;

public:

    HTTPResponse();

    void setResponseStatusCode (const string & aResponseStatusCode );
    void setCacheControl( enCacheControl_t aCahceControl);
    void setContentType(const string & aContentType);
    void setCookie( HTTPCookie & aCookie);

    void appendHeaderField(const string & aHeaderField);

    void clearContent();
    void clearHTTPHeader();
    void clear();

    void write (const string & aResponseData);
    void write ( const char * buff, size_t n );
    void writeFile( const string & aFileContent, const string & aContentType );

    void buildHTTPResponse( char ** apBuffer , size_t & OUT aSize );
    virtual ~HTTPResponse () {}

private:

    void buildHTTPResponseHeader(string & IN OUT aResponseHeader);
    void getContentSize( string & IN OUT aContentSize ) const;
};

}


#endif // HTTPRESPONSE_H_INCLUDED
