#ifndef UTILITIES_H_INCLUDED
#define UTILITIES_H_INCLUDED

#include <string>
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <uuid/uuid.h>

using namespace std;

namespace  utilities {

    static  int stringToInteger(const string & strValue) {

        string value = strValue;

        if (!value.compare("")) {
            value = "0";
        }

        int  intBuff;
        stringstream (value) >> intBuff;
        return intBuff;
    }

    static  string integerToString(int intValue) {

        stringstream ss;
        ss << intValue;
        return ss.str();
    }

    static string generateUUID( )
    {
        char cookie[37];

        uuid_t uuidValue;
        uuid_generate_random( uuidValue );
        uuid_unparse( uuidValue, cookie );

        return string(cookie);
    }

}

#endif // UTILITIES_H_INCLUDED
