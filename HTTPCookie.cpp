#include "HTTPCookie.h"

namespace HTTP {

HTTPCookie::HTTPCookie ( ) {
    setExpirationDateUTC("Wed, 01-Jan-2070 00:00:00 GMT;");
}

HTTPCookie::HTTPCookie (const string & aCookieID ) {
    setCookieID(aCookieID);
    setExpirationDateUTC("Wed, 01-Jan-2070 00:00:00 GMT;");
}

void HTTPCookie::setCookieID (const string & aCookieID) {
    m_cookieID = aCookieID;
}

void HTTPCookie::setDomain (const string & aDomain) {
    m_domain = aDomain;
}

void HTTPCookie::setExpirationDateUTC (const string & aExpire) {
    m_expire = aExpire;
}

void HTTPCookie::buildHTTPResponseHeaderFields( string & IN OUT aHttpResponseHeaderField ) {

    aHttpResponseHeaderField = "";
    aHttpResponseHeaderField.append( "Set-Cookie: " );
    aHttpResponseHeaderField.append( "asid=" );
    aHttpResponseHeaderField.append( m_cookieID );
    aHttpResponseHeaderField.append( "; Domain=" );
    aHttpResponseHeaderField.append( m_domain );
    aHttpResponseHeaderField.append( "; Expires=" );
    aHttpResponseHeaderField.append( m_expire );
    aHttpResponseHeaderField.append(" HttpOnly\r\n");
}

}
