MAKE 	= make
MFLAGS	= -C
CC	= g++
PRJCFLAGS = -ggdb
RM	= rm
ECHO	= echo
DIRS 	= http_parser uv 
EXE 	= systimeserver
OBJS	= http_parser/http_parser.o 
OBJLIBS = uv/uv.a
LIBS	= -luuid
SOURCES = runserver.cpp  HTTPServer.cpp HTTPContext.cpp uvHTTPContext.cpp HTTPRequest.cpp HTTPResponse.cpp HTTPCookie.cpp URL.cpp HTTPHandlerFactory.cpp HTTPErrorHandler.cpp HTTPSystemTimeHandler.cpp

.SILENT :

all: $(EXE);

$(EXE): $(OBJS) $(OBJLIBS)
	$($ECHO) $(CC) $(PRJCFLAGS) $(SOURCES) $(OBJS) $(OBJLIBS) $(LIBS) -o $(EXE)

uv/uv.a: force_look 
	$(ECHO) executing : $(MAKE) $(MFLAGS) uv
	$(MAKE) $(MFLAGS) uv 

http_parser/http_parser.o: force_look
	$(ECHO) executing : $(MAKE) $(MFLAGS) http_parser
	$(MAKE) $(MFLAGS) http_parser http_parser.o

clean:
	$(ECHO) cleaning up...
	-$(RM) -f $(EXE)
	-for d in $(DIRS); do (cd $$d; $(MAKE) clean ); done

force_look : 
	true
