#ifndef HTTPSERVER_H_INCLUDED
#define HTTPSERVER_H_INCLUDED

#pragma once

#include <string>
#include <map>
#include <fstream>
#include <streambuf>
#include <iostream>
#include <stdlib.h>
#include <ctime>

#include "http_parser/http_parser.h"
#include "uv/uv.h"

#include "Exceptions.h"

using namespace std;


namespace HTTP {

static string lastFieldName;


class HTTPServer
{
    ///  HTTP PARSER RELATED HANDLERS ///
    static  int onHeadersComplete( http_parser* httpParser );
    static  int onMessageComplete( http_parser* httpParser );
    static  int onQuery( http_parser* httpParser, const char *at, size_t length );
    static  int onFragment( http_parser* httpParser, const char *at, size_t length );
    static  int onField( http_parser* httpParser, const char *at, size_t length );
    static  int onValue( http_parser* httpParser, const char *at, size_t length );
    static  int onBody( http_parser* httpParser, const char *at, size_t length );
    static  int onURL( http_parser* httpParser, const char *at, size_t length );

    /// UV LIB RELATED HANDLERS ///
    static  void    onConnect( uv_tcp_t* server, int status );
    static  uv_buf_t    onAlloc( uv_tcp_t* handle, size_t suggestedSize );
    static  void    onRead( uv_tcp_t* handle, int nread, uv_buf_t buf );
    static  void    onClose( uv_handle_t* handle );
    static  void    onAfterWrite( uv_req_t* req, int status );


    string    m_hostName;
    int       m_port;

    HTTPServer () {};

    /// TODO :: check uv_tcp_t data type and then implement the copy constr and assigmnet operator
    HTTPServer ( const HTTPServer & aHttpServer ) {}
	HTTPServer & operator=(const HTTPServer &aHTTPServer ) {}

public:

    static http_parser_settings     m_parserSettings;
	uv_tcp_t*                       m_pServer;

	HTTPServer( const string & aHostName, int aPort ) throw ();

	const string & HostName() { return m_hostName; }
	uint  Port()  { return m_port; }
    void run( ) throw () ;
	virtual ~HTTPServer( );
};

}

#endif // HTTPSERVER_H_INCLUDED
