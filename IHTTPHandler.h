#ifndef HTTPHANDLER_H_INCLUDED
#define HTTPHANDLER_H_INCLUDED

#include "HTTPContext.h"

namespace HTTP {

struct IHTTPHandler {

    virtual void processRequest (HTTPContext & context) = 0;
};

}

#endif // HTTPHANDLER_H_INCLUDED
