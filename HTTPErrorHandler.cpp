#include "HTTPErrorHandler.h"

namespace HTTP {

void HTTPErrorHandler::processRequest( HTTPContext & context ) {
       context.Response().setResponseStatusCode( "404 Not Found" );
}

}
