#include "URL.h"

#include <iostream>
#include "Manifest.h"


namespace HTTP {

URL :: URL(const string & aUrl) {

    if ( aUrl.empty() ) return;

    m_enRequestType = en_RequestType_None;

    m_url = aUrl;
    size_t queryPosition = aUrl.find( '?' );
    string urlPath = m_url;
    if ( string::npos != queryPosition ) {
        urlPath = m_url.substr( 0, queryPosition );
        setURLParams ( m_url.substr( queryPosition +1 ) );
    }

    size_t found = urlPath.find_last_of("/");

    if ( found != string::npos ) {

        m_resource = urlPath.substr(found+1);
        m_enRequestType =  ( string::npos != m_resource.find_last_of(".") ) ? ( ( string::npos != m_resource.find (".html") ) ? (en_RequestType_WebPage) : (en_RequestType_File) )
                                                                        : (en_RequestType_Service);
    }
    // TODO :: process another URL Parts
}

void URL::setURLParams (const string & aUrlQuery) {

    size_t current = 0;
    size_t last = 0;
    string temp;

    m_query = aUrlQuery;

    do {
        if( string::npos == current ) {
            break;
        } else if( current > 0 ) {
            last = current + 1;
        }

        current = aUrlQuery.find( '&', current + 1 );
        if( string::npos == current ) {
            temp = aUrlQuery.substr( last );
        } else {
            temp = aUrlQuery.substr( last, current - last );
        }

        size_t separator = temp.find( '=' );

        if( string::npos != separator ) {
            m_UrlProperties.insert( URLProperties::value_type(
            temp.substr( 0, separator ),
            temp.substr( separator + 1 )
            ) );
        }

    } while( string::npos != last );
}

const string & URL :: Url() const {
    return m_url;
}

const string & URL :: Protocol() const {
    return m_protocol;
}

const string & URL :: Host() const {
    return m_host;
}

const string & URL :: Port() const {
    return m_port;
}

const string & URL :: FilePath() const {
    return m_filePath;
}

const string & URL :: Query () const {
    return m_query;
}

const string & URL:: Resource () const {
    return m_resource;
}


const string & URL::operator [] (const string & aKey) const {

    URLProperties::const_iterator it  = m_UrlProperties.find( aKey );
    if( m_UrlProperties.end( ) != it ) {
        return it->second;
    }
    return const_string;
}

en_RequestType_t URL:: ResourceType () const {
    return m_enRequestType;
}

} // NAMESPACE END
