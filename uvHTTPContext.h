#ifndef UVHTTPCONTEXT_H_INCLUDED
#define UVHTTPCONTEXT_H_INCLUDED

#include "HTTPContext.h"
#include "http_parser/http_parser.h"
#include "uv/uv.h"

namespace HTTP {

    class uvHTTPContext : public HTTPContext {

    public:
        uv_tcp_t        m_uvHandle;
        uv_req_t        m_writeRequest;
        uv_buf_t        m_ResponseBuffer;
        http_parser     m_httpParser;

        uvHTTPContext ();
        uvHTTPContext ( const uvHTTPContext & aUVContext );
        uvHTTPContext & operator= ( const uvHTTPContext & aUVContext);

        virtual ~uvHTTPContext ();
    };
}



#endif // UVHTTPCONTEXT_H_INCLUDED
