#include "HTTPSystemTimeHandler.h"
#include <iostream>
#include <sstream>
#include <time.h>

using namespace std;

namespace HTTP {

void HTTPSystemTimeHandler :: processRequest( HTTPContext & context ) {

    stringstream ss;
    time_t t;
    time( &t);
    ss << t;
    context.Response().write( ss.str() );

}

}
