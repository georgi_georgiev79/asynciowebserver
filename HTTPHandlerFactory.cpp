#include "HTTPHandlerFactory.h"
#include "HTTPRequest.h"
#include "HTTPSystemTimeHandler.h"

#define ERROR_MESSAGE_RESOURCE_UNAVAILABLE  "Requested resource or service is unavailable!"


namespace HTTP {

HTTPHandlerMap HTTPHandlerFactory::m_webServiceHandlerMap;
HTTPErrorHandler HTTPHandlerFactory::m_errorHandler;

HTTPHandlerFactory::HTTPHandlerFactory () {

    if ( m_webServiceHandlerMap.empty() ) {
        m_webServiceHandlerMap.insert( HTTPHandlerMap::value_type("systemtime", new HTTPSystemTimeHandler() ) );
    }
}


IHTTPHandler * HTTPHandlerFactory :: createHandler ( const HTTPRequest & request ) {

    const string & resource =  request.Url().Resource();
    HTTPHandlerMap::iterator iter;

    switch ( request.Url().ResourceType() ) {

        case en_RequestType_File :
            return ( IHTTPHandler * ) & m_errorHandler;
        break;

        case en_RequestType_WebPage :
           return ( IHTTPHandler * ) & m_errorHandler;
        break;

        case en_RequestType_Service :
            iter = m_webServiceHandlerMap.find( resource );
            if( m_webServiceHandlerMap.end( ) != iter ) {
                return (iter->second);
            }
        break;
    }

    return ( IHTTPHandler * ) & m_errorHandler;
}


void HTTPHandlerFactory::dealloc () {

    HTTPHandlerMap::iterator iter;

    if (! m_webServiceHandlerMap.empty() ) {
         for( iter = m_webServiceHandlerMap.begin(); m_webServiceHandlerMap.end() != iter; iter++ ) {
            delete (iter->second);
            m_webServiceHandlerMap.erase(iter);
        }
    }
}

} // namespace end
