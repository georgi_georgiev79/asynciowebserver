#ifndef EXCEPTIONS_H_INCLUDED
#define EXCEPTIONS_H_INCLUDED

#include <exception>
#include <string>

using namespace std;


class Exception : public exception {

    string m_message;

    protected :
    Exception () {}

    public:

        Exception ( const string & aMessage ) : exception() {
            m_message = aMessage;
        }

        virtual const char * what () const throw() {
            return m_message.c_str();
        }

        virtual ~Exception () throw () {}
};


class ValidationException : public Exception {

    ValidationException() {}

    public:
        ValidationException ( const string & aMessage ) : Exception(aMessage) {}
};



class UVException : public Exception {

    UVException(){}

    public:

        UVException ( const string & aMessage ) : Exception(aMessage) {}
};

class HTTPPareserException : public Exception {

    HTTPPareserException() {}

    public:

        HTTPPareserException ( const string & aMessage ) : Exception(aMessage) {}
};


#endif // EXCEPTIONS_H_INCLUDED
