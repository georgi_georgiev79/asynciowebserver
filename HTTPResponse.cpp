#include "HTTPResponse.h"
#include "utilities.h"
#include <exception>
#include <iostream>

namespace HTTP {

HTTPResponse::HTTPResponse () {

    setResponseStatusCode("200 OK");
    m_headerFields = string("");
    setCacheControl(enCacheControl_NoCache);
}

void HTTPResponse::setResponseStatusCode (const string & aResponseStatusCode ) {
    m_responseStatusCode = "HTTP/1.1 " + aResponseStatusCode + "\r\n";
}

void HTTPResponse::setCacheControl( enCacheControl_t aCahceControl) {

    m_cacheControl = "";

    switch (aCahceControl) {

        case enCacheControl_NoCache:
            m_cacheControl.append( "Cache-Control: no-cache\r\n" );
            m_cacheControl.append( "Cache-Control: no-store\r\n" );
            m_cacheControl.append( "Pragma: no-cache\r\n" );
            m_cacheControl.append( "Expires: Thu, 01 Jan 1970 00:00:00 GMT\r\n" );
        break;

        case enCacheControl_Private:
            // TODO::
        break;

        case enCacheControl_Server :
             // TODO::
        break;

        case enCacheControl_ServerAndNoCache:
             // TODO::
        break;

        case enCacheControl_Public :
            m_cacheControl.append( "Cache-Control:public, max-age=300\r\n" );
        break;

        case enCacheControl_ServerAndPrivate:
            // TODO::
        break;
    }

}

void HTTPResponse::setContentType(const string & aContentType) {

    m_contentType = "";
    m_contentType.append( "Content-Type: " );
    m_contentType.append( aContentType );
    m_contentType.append( "\r\n" );

}

void HTTPResponse :: setCookie(HTTPCookie & aCookie) {

    aCookie.buildHTTPResponseHeaderFields( IN OUT m_cookieHeaders);
}

void HTTPResponse::appendHeaderField(const string & aHeaderField) {
    m_headerFields.append(aHeaderField);
}

void HTTPResponse::clearContent() {
      m_responseBody = "";
}

void HTTPResponse::clearHTTPHeader() {
    m_contentType = "";
    m_cookieHeaders = "";
    m_cacheControl = "";
    m_headerFields = "";
}

void HTTPResponse::clear() {
    setResponseStatusCode("");
    clearHTTPHeader();
    clearContent();
}

void HTTPResponse::write (const string & aResponseData) {
    m_responseBody += aResponseData;
}

void HTTPResponse :: write (const char * buff, size_t n) {
    m_responseBody.append( buff, n );
}

void HTTPResponse:: writeFile( const string & aFileContent, const string & aContentType ) {

    setContentType(aContentType);
    m_responseBody += aFileContent;
}

void HTTPResponse :: buildHTTPResponse( char ** apBuffer , size_t & OUT aSize ) {

    string httpResponseHeader;
    buildHTTPResponseHeader( IN OUT httpResponseHeader );

    string httpResponse = m_responseStatusCode + httpResponseHeader + m_responseBody + "\r\n";

    aSize = httpResponse.length() ;
    * apBuffer = new char[ aSize ];

    memcpy( ( * apBuffer ) , httpResponse.c_str( ), httpResponse.length() );

}
void HTTPResponse :: buildHTTPResponseHeader(string & IN OUT aResponseHeader) {

    aResponseHeader = "";
    string contentSize;
    getContentSize (contentSize);
    aResponseHeader = m_cookieHeaders + m_contentType + contentSize + m_cacheControl + "\r\n";
}

void HTTPResponse :: getContentSize( string & IN OUT aContentSize ) const {

    aContentSize = "";
    aContentSize += "Content-Length:";
    aContentSize += utilities::integerToString( m_responseBody.length() );
    aContentSize += "\r\n";
}

} // HTTPServer namespace end





