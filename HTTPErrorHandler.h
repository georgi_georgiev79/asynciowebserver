#ifndef HTTPERRORHANDLER_H_INCLUDED
#define HTTPERRORHANDLER_H_INCLUDED

#include "IHTTPHandler.h"

namespace HTTP {

class HTTPErrorHandler : public IHTTPHandler {

public:
    virtual void processRequest(HTTPContext & context);
};

}



#endif // HTTPERRORHANDLER_H_INCLUDED
