#include "uvHTTPContext.h"
#include <cstring>
#include "Manifest.h"

namespace HTTP {

uvHTTPContext :: uvHTTPContext () : HTTPContext () {
      m_ResponseBuffer.base = NULL;
}

uvHTTPContext :: uvHTTPContext ( const uvHTTPContext & aUVContext ) : HTTPContext () {

    /// TODO ::  should be implemnted
    /// check data types :
    // uv_tcp_t
    // uv_req_t
    // http_parser

    m_ResponseBuffer.len = aUVContext.m_ResponseBuffer.len;
    m_ResponseBuffer.base = new char[ aUVContext.m_ResponseBuffer.len ];

    memcpy( m_ResponseBuffer.base , aUVContext.m_ResponseBuffer.base, aUVContext.m_ResponseBuffer.len );
}

uvHTTPContext & uvHTTPContext ::  operator= ( const uvHTTPContext & aUVContext) {

    if ( this == & aUVContext ) return * this;

    RELEASE (m_ResponseBuffer.base );


    /// TODO ::  should be implemnted
    /// check data types :
    // uv_tcp_t
    // uv_req_t
    // http_parser

    m_ResponseBuffer.len = aUVContext.m_ResponseBuffer.len;
    m_ResponseBuffer.base = new char[ aUVContext.m_ResponseBuffer.len ];

    memcpy( m_ResponseBuffer.base , aUVContext.m_ResponseBuffer.base, aUVContext.m_ResponseBuffer.len );

}

uvHTTPContext :: ~uvHTTPContext () {

    if (m_ResponseBuffer.base != NULL ) {
        delete m_ResponseBuffer.base;
    }
}

}
