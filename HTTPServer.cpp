#include "HTTPServer.h"

#include <signal.h>
#include <stdlib.h>
#include <exception>

#include "uvHTTPContext.h"
#include "HTTPHandlerFactory.h"

#include "Manifest.h"
#include "utilities.h"


namespace HTTP {

http_parser_settings    HTTPServer::m_parserSettings;


HTTPServer::HTTPServer( const string & aHostName, int aPort ) throw ()
{

    if ( ( HTTPServer::m_parserSettings.on_message_complete == NULL ) ) {
        HTTPServer::m_parserSettings.on_header_field = HTTPServer::onField;
        HTTPServer::m_parserSettings.on_header_value = HTTPServer::onValue;
        HTTPServer::m_parserSettings.on_body = HTTPServer::onBody;
        HTTPServer::m_parserSettings.on_url = HTTPServer::onURL;
        HTTPServer::m_parserSettings.on_headers_complete = HTTPServer::onHeadersComplete;
        HTTPServer::m_parserSettings.on_message_complete = HTTPServer::onMessageComplete;
    }

    //TODO :: add more sofisticated validation Validation
    if ( aHostName.empty() ||  ( aPort <= 0 )  ) {
        throw ValidationException ( "Invalid params aHostName or aPort in HTTPServer constructor!" );
    }

    m_hostName = aHostName;
    m_port = aPort;

    uv_init();
    m_pServer = new uv_tcp_t;
}

void HTTPServer::run( ) throw ()
{
    uv_tcp_init( m_pServer );

    int result = uv_bind( m_pServer,
                          uv_ip4_addr(  (char*)(m_hostName.c_str( )) , Port() )
                 );

    if( result ) {
        throw UVException( "UV Bind Error :: " + string ( uv_strerror( uv_last_error( ) ) ) );
    }

    result = uv_listen( m_pServer, 8192, onConnect );

    if( result ) {
        throw UVException( "UV Listen Error :: " + string ( uv_strerror( uv_last_error( ) ) ) );
    }

    uv_run( );
}

HTTPServer::~HTTPServer( )
{
    RELEASE (m_pServer);
}

/// UV LIB Event Handlers BEGIN

void HTTPServer :: onConnect( uv_tcp_t* server, int status )
{

    if( !status && server ) {

        uvHTTPContext * pUVContext = new uvHTTPContext ();
        int resultCode = uv_accept( server, &( pUVContext->m_uvHandle ) );

        if( resultCode ) {
            ///TODO :: LOG ERROR in LOG file -> uv_strerror( uv_last_error( ) )
            cout << "UV Connect Error :: " <<  uv_strerror( uv_last_error( ) ) << endl;
            delete pUVContext;
            return;
        }

        pUVContext->m_uvHandle.data = pUVContext;
        pUVContext ->m_httpParser.data = pUVContext;
        http_parser_init( &(pUVContext->m_httpParser), HTTP_REQUEST );
        uv_read_start( &(pUVContext->m_uvHandle ), onAlloc, onRead );
    }

}

void HTTPServer :: onRead( uv_tcp_t* handle, int nread, uv_buf_t buf ) {

    uvHTTPContext *pUVContext = (uvHTTPContext * )handle->data;
    size_t parsed;

    if( nread >= 0 ) {
        parsed = http_parser_execute(   &(pUVContext->m_httpParser ),
                                        &(HTTPServer::m_parserSettings),
                                        buf.base, nread );

        if( parsed < nread ) {
             ///TODO :: LOG ERROR -> HTTP PArser
            cout << "UV Read Error :: parsed bytes" << endl;
            uv_close( (uv_handle_t*)handle, onClose );
        }
    } else {
        uv_err_t err = uv_last_error( );

        if( err.code != UV_EOF ) {
           cout << "UV Read Error" << endl;
        }

        uv_close( (uv_handle_t*)handle, onClose );
    }

    free( buf.base );
}

uv_buf_t HTTPServer :: onAlloc( uv_tcp_t* handle, size_t suggestedSize )
{
    uv_buf_t buf;
    buf.base = (char*)malloc(suggestedSize);
    buf.len = suggestedSize;

    return buf;
}

void HTTPServer :: onAfterWrite( uv_req_t* req, int status ) {
    uv_close( req->handle, onClose );
}

void HTTPServer :: onClose( uv_handle_t* handle ) {
    delete (uvHTTPContext * ) handle->data;
}

/// UV LIB END

/////////////////////////////////////////////////////////////////////////////////////////

/// HTTP PARSER Handlers BEGIN

int HTTPServer :: onURL( http_parser* httpParser, const char *at, size_t length ) {
    uvHTTPContext * pUVContext = (uvHTTPContext *)httpParser->data;
    ( ( HTTPRequest &) pUVContext->Request() ).Url( new URL(string(at,length)));

    return 0;
}

int HTTPServer :: onQuery( http_parser* httpParser, const char *at, size_t length ) {
    return 0;
}

int HTTPServer :: onField( http_parser* httpParser, const char *at, size_t length ) {
    lastFieldName.assign(at,length);
    return 0;
}

int HTTPServer :: onValue( http_parser* httpParser, const char *at, size_t length ) {
    uvHTTPContext * pUVContext = (uvHTTPContext * )httpParser->data;
    ( ( HTTPRequest & ) pUVContext->Request() ).setHttpPropertiesKeyWithValue( lastFieldName, string(at, length) );

    return 0;
}

int HTTPServer::onHeadersComplete( http_parser* httpParser ) {

    uvHTTPContext * pUVContext = (uvHTTPContext * )httpParser->data;
    string cookieID =  pUVContext->Request()[HTTPREQUEST_COOKIE];

    if ( cookieID.empty() ) {
        cookieID = utilities::generateUUID();
       ( ( HTTPRequest & )  pUVContext->Request() ).setCookie( cookieID );
        HTTPCookie cookie( cookieID );
       ///  cookie.setDomain( "http://systemtime.com" );
        pUVContext->Response().setCookie( cookie );
   }

    return 0;
}

int HTTPServer :: onBody( http_parser* httpParser, const char *at, size_t length ) {
    uvHTTPContext * pUVContext = (uvHTTPContext * ) httpParser->data;
    ( ( HTTPRequest & ) pUVContext->Request() ).PostData( at, length );

    return 0;
}

int HTTPServer :: onMessageComplete( http_parser* httpParser ) {

    uvHTTPContext * pUVContext = ( uvHTTPContext *)httpParser->data;

    HTTPHandlerFactory handlerFactory;

    IHTTPHandler * pHttpHandler = (IHTTPHandler *) handlerFactory.createHandler( pUVContext->Request() );
    pHttpHandler->processRequest( ( ( HTTPContext & ) (*pUVContext) ) );

    pUVContext->Response().buildHTTPResponse( &pUVContext->m_ResponseBuffer.base, pUVContext->m_ResponseBuffer.len );

    uv_req_init(    &(pUVContext->m_writeRequest),
                    (uv_handle_t*)(&(pUVContext->m_uvHandle )),
                    (void *)onAfterWrite );

    uv_write( &(pUVContext->m_writeRequest), &(pUVContext->m_ResponseBuffer), 1 );

    return 0;
}

/// HTTP PARSER END


}
