#ifndef URL_H_INCLUDED
#define URL_H_INCLUDED

#include <string>
#include <map>

using namespace std;

namespace HTTP {

typedef map<string , string> URLProperties;

enum en_RequestType_t {
    en_RequestType_None = 0,
    en_RequestType_File,
    en_RequestType_WebPage,
    en_RequestType_Service
};

class URL  {

    string m_url;
    string m_protocol;
    string m_host;
    string m_port;
    string m_filePath;
    string m_resource;
    string m_query;

    URLProperties m_UrlProperties;
    en_RequestType_t m_enRequestType;

    void setURLParams (const string & aUrlQuery);

    URL() {}

public:

    URL(const string & url);

    const string & Url() const;
    const string & Protocol() const;
    const string & Host() const;
    const string & Port() const;
    const string & FilePath() const;
    const string & Query () const;
    const string & Resource () const;
    en_RequestType_t ResourceType () const;

    /// provides access to url query parameters
    const string & operator [] (const string & aKey ) const;

};

}
#endif // URL_H_INCLUDED
