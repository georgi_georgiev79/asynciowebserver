#include "uv/uv.h"
#include <iostream>
#include "HTTPServer.h"
#include "utilities.h"

using namespace HTTP;

int main( int argc, char* argv[] )
{
    try {

        HTTPServer httpServer ( string(argv[1]) , utilities::stringToInteger( string (argv[2]) ) );
        httpServer.run();

    } catch (const ValidationException & e) {
        cout << "Validation Exception :: " << e.what() << endl;
    } catch ( const UVException & e ) {
        cout << "UV LIB Exception :: " << e.what() << endl;
    } catch ( ... ) {
        cout << "Unexpectet Error has occured !" << endl;
    }

    return 0;
}
