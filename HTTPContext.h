#ifndef HTTPCONTEXT_H_INCLUDED
#define HTTPCONTEXT_H_INCLUDED

#include "HTTPResponse.h"
#include "HTTPRequest.h"

namespace HTTP {


class HTTPContext  {

    HTTPRequest m_request;
    HTTPResponse m_response;

    /// TODO :: DEFINE  CACHE & SESSION OBJECTS

public:

    HTTPContext () {}
    HTTPContext (const HTTPContext & aContext);
    HTTPContext & operator= ( const HTTPContext & aContext );

    HTTPResponse & Response();
    const HTTPRequest & Request ();

    virtual ~HTTPContext();
};

}

#endif // HTTPCONTEXT_H_INCLUDED
