#ifndef HTTPREQUEST_H_INCLUDED
#define HTTPREQUEST_H_INCLUDED

#include <string>
#include <map>
#include "URL.h"

#define HTTPREQUEST_COOKIE              "Cookie"
///  TODO :: Add here more properties

using namespace std;

namespace HTTP {

typedef map<string , string> HTTPRequestProperties;

class HTTPRequest  {

    URL * m_pURL;
    HTTPRequestProperties m_httpProperties;
    string m_postData;

public:

    HTTPRequest();
    HTTPRequest (const HTTPRequest &);
    HTTPRequest & operator=(const HTTPRequest &);

    const URL & Url() const;
    void Url ( const URL * apURL );

    const string & operator [] (const string & aKey ) const;
    void setHttpPropertiesKeyWithValue (const string & aKey, const string & aValue);

    const string & PostData() const;
    void PostData( const string & aPostData );
    void PostData ( const char * pBuff, size_t n );
    void setCookie(const string & cookieID);

    virtual ~HTTPRequest ();
};

}

#endif // HTTPREQUEST_H_INCLUDED
