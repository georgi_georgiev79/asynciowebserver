#ifndef HTTPSYSTEMTIMEHANDLER_H_INCLUDED
#define HTTPSYSTEMTIMEHANDLER_H_INCLUDED

#include "IHTTPHandler.h"

namespace HTTP {

class HTTPSystemTimeHandler : public IHTTPHandler {

    public:
    virtual void processRequest ( HTTPContext & context );


};

}

#endif // HTTPSYSTEMTIMEHANDLER_H_INCLUDED
