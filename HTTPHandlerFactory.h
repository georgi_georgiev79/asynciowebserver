#ifndef HTTPHANDLERFACTORY_H_INCLUDED
#define HTTPHANDLERFACTORY_H_INCLUDED

#include "IHTTPHandler.h"
#include "HTTPErrorHandler.h"
#include <map>
#include <string>

using namespace std;


namespace HTTP {

typedef map <string, IHTTPHandler * > HTTPHandlerMap;

class HTTPHandlerFactory {

    static HTTPHandlerMap m_webServiceHandlerMap;
    static HTTPErrorHandler m_errorHandler;

    public:
    HTTPHandlerFactory ();
    IHTTPHandler * createHandler ( const HTTPRequest & request );

    static void dealloc ();
};

}

#endif // HTTPHANDLERFACTORY_H_INCLUDED
