#include "HTTPContext.h"

namespace HTTP {

HTTPContext :: HTTPContext (const HTTPContext & aContext) {

    m_request = aContext.m_request;
    m_response = aContext.m_response;

     /// TODO :: IF CACHE OR SESSION OBJECTS ARE POINTERS COPY THEM HERE
}

HTTPContext &  HTTPContext :: operator= ( const HTTPContext & aContext ) {

    if (this ==  &aContext)  return *this;

    m_request = aContext.m_request;
    m_response = aContext.m_response;

    /// TODO :: IF CACHE OR SESSION OBJECTS ARE POINTERS COPY THEM HERE

    return *this;
}

HTTPResponse & HTTPContext :: Response() {
    return m_response;
}

const HTTPRequest & HTTPContext :: Request () {
    return m_request;
}

HTTPContext :: ~HTTPContext () {

    /// TODO :: IF CACHE OR SESSION OBJECTS ARE POINTERS RELEASE HERE
}

}// namspace end
